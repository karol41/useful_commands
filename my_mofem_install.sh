#!/bin/bash

# spack install pkg-config doxygen +gnuplot+X && spack install ffmpeg && spack install parallel 
# spack install --only dependencies mofem-cephas+slepc ^petsc+X
cd $HOME
# spack view --verbose symlink -i ~/spack_view doxygen petsc moab metis boost adol-c tetgen med slepc cmake parallel gnuplot mgis
#  spack view remove ~/spack_view petsc moab boost adol-c tetgen slepc
#  cp ~/Projects_ALL/ffmpeg/ffmpeg $SPACKVIEW/bin
# ln -s /Applications/Octave-5.2.0.app/Contents/Resources/usr/bin/octave-octave-app\@5.2.0 ~/spack_view/bin/octave

spack view --verbose symlink -i ~/spack_view doxygen petsc moab metis boost adol-c tetgen med slepc cmake parallel gnuplot mgis 
export MOFEM_INSTALL_DIR=$HOME/mofem_install
export SPACKVIEW=$HOME/spack_view/
mkdir $MOFEM_INSTALL_DIR
cd $MOFEM_INSTALL_DIR 
git clone https://karol41@bitbucket.org/likask/mofem-cephas.git

mkdir lib
cd lib
# RelWithDebInfo Debug Release
#configure and install library
cmake -DCMAKE_INSTALL_PREFIX=$MOFEM_INSTALL_DIR/users_modules \
    -DCMAKE_BUILD_TYPE=Release \
    -DPETSC_DIR=$SPACKVIEW \
    -DMOAB_DIR=$SPACKVIEW \
    -DBOOST_DIR=$SPACKVIEW \
    -DBOOST_ROOT=$SPACKVIEW \
    -DBOOST_INCLUDEDIR=$SPACKVIEW/include/boost \
    -DBOOST_LIBRARYDIR=$SPACKVIEW/lib \
    -DMOFEM_BUILD_TESTS=ON \
    -DADOL-C_DIR=$SPACKVIEW \
    -DTETGEN_DIR=$SPACKVIEW \
    -DMED_DIR=$SPACKVIEW \
    -DSLEPC_DIR=$SPACKVIEW  $MOFEM_INSTALL_DIR/mofem-cephas/mofem
make -j6 install
cd ../users_modules/
#configure and install users modules
export MGIS_PATH=$SPACKVIEW/lib
export MGIS_PATH=$(spack find -l --path mgis | awk 'END{print}' | awk 'NF{ print $NF }')"/lib"
cmake -DCMAKE_BUILD_TYPE=Release -DWITH_METAIO=1 -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DMOFEM_UM_BUILD_TESTS=1 -DMoFEM_DIR=$MOFEM_INSTALL_DIR/users_modules -DCMAKE_EXE_LINKER_FLAGS="-L$MOFEM_INSTALL_DIR/users_modules/" -DMGIS_DIR=$MGIS_PATH -DCMAKE_INSTALL_PREFIX=$SPACKVIEW/bin $MOFEM_INSTALL_DIR/mofem-cephas/mofem/users_modules
make -j6

# DEBUG
mkdir ../lib_debug/
cd ../lib_debug/
cmake -DCMAKE_INSTALL_PREFIX=$MOFEM_INSTALL_DIR/users_modules_debug \
    -DCMAKE_BUILD_TYPE=Debug \
    -DPETSC_DIR=$SPACKVIEW \
    -DMOAB_DIR=/Users/karollewandowski/spack/opt/spack/darwin-catalina-skylake/apple-clang-11.0.0/moab-master-pj34yi7cbeyl6q7sqdp7r2swona5porm \
    -DBOOST_DIR=$SPACKVIEW \
    -DBOOST_ROOT=$SPACKVIEW \
    -DBOOST_INCLUDEDIR=$SPACKVIEW/include/boost \
    -DBOOST_LIBRARYDIR=$SPACKVIEW/lib \
    -DMOFEM_BUILD_TESTS=ON \
    -DADOL-C_DIR=$SPACKVIEW \
    -DTETGEN_DIR=$SPACKVIEW \
    -DMED_DIR=$SPACKVIEW \
    -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
    -DSLEPC_DIR=$SPACKVIEW  $MOFEM_INSTALL_DIR/mofem-cephas/mofem
make -j6 install

export MGIS_PATH=$SPACKVIEW/lib
cd ../users_modules_debug/
cmake -DCMAKE_BUILD_TYPE=Debug -DWITH_METAIO=1 -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DMOFEM_UM_BUILD_TESTS=1 -DMoFEM_DIR=$MOFEM_INSTALL_DIR/users_modules_debug -DMGIS_DIR=$MGIS_PATH -DCMAKE_EXE_LINKER_FLAGS="-L/$MOFEM_INSTALL_DIR/users_modules_debug/" $MOFEM_INSTALL_DIR/mofem-cephas/mofem/users_modules

make -j6


for f in add_meshsets convert.py delete_ho_nodes  extrude_prisms  field_to_vertices mesh_cut meshset_to_vtk mesh_smoothing mofem_part read_med split_sideset uniform_mesh_refinement; do 
ln -s  $MOFEM_INSTALL_DIR/users_modules/tools/$f  $SPACKVIEW/bin
done


spack view --verbose symlink -i ~/spack_view $PACKAGE