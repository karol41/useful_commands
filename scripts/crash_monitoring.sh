#!/bin/bash

LOG_FILE_NAME="log"
tail -n100 -f $LOG_FILE_NAME | \
while read line ; do
        echo "$line" | grep "ERROR"
        if [ $? = 0 ]
        then
                echo "There is an error."

                ## Send message using Slack API
                URL="https://slack.com/api/chat.postMessage"
                CONTENT_TYPE="accept: application/json"
                TOKEN_ID=xoxp-6042012464-15529708597-577917570165-1d395057beaad1be0e7d1fe8b8d160c1
                CHANNEL_ID=YOUR_CHANNEL_ID_HERE
                BASE_NAME=basename "$PWD"
                MESSAGE="ANALYSIS ERROR - DIR: "${PWD##*/}""
                curl -X POST $URL -H  "$CONTENT_TYPE" -d \
                token=$TOKEN_ID -d channel=$CHANNEL_ID -d text="$MESSAGE"

                ## Send message using Telegram API
                #URL="https://api.telegram.org/bot...."
                #CHAT_ID=YOUR_CHAT_ID_HERE
                #BASE_NAME=basename "$PWD"
                #MESSAGE="ANALYSIS ERROR - DIR: "${PWD##*/}""
                #curl -s -X POST $URL -d \
                #chat_id=$CHAT_ID -d text="$MESSAGE"

                break
        fi
done

