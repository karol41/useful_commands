#!/bin/bash

input=$1
touch tmp.txt
iter=0
while IFS= read -r line
do
	if [ $iter -eq 1 ]
 then
		echo "0,0" >> tmp.txt
	fi
	echo "$line" >> tmp.txt

  cat tmp.txt > my_graph_${iter}.txt
  iter=$(($iter + 1))
done < "$input"

rm tmp.txt
