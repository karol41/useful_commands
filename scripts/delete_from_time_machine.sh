#!/bin/bash
# https://www.macworld.com/article/2033804/control-time-machine-from-the-command-line.html
paths=$(find ~/moFEM -name ipch)
for file in $paths
do 
echo $file
sudo tmutil addexclusion -p $file
done

# to check
# sudo mdfind "com_apple_backup_excludeItem = 'com.apple.backupd'"