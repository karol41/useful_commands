#!/bin/bash

for f in *.cub
do
base=${f%.cub};
echo "$base"
 mpirun -np 8 ../mass_calculation -meta_image mesh${base#"surface"}.mhd -my_file ${f} -param_density 1,0 -cube_size 3 -dist_tol 0.5 -my_order 3 -lambda 0.1 -ksp_type cg -pc_type lu -pc_factor_mat_solver_package mumps | tee -a log1
mbconvert out.h5m ${base}.vtk
done

