#!/bin/bash


order=3
rm log*
for proc in 1 2 4 8 12 
do
../tools/mofem_part -my_file bone_test_no_screw2.cub -my_nparts ${proc}

echo 'STANDARD'
for var in 0 1 2
do
mpirun -mca oob_tcp_keepalive_time 0 -np ${proc} ./bone_adaptation -my_file out.h5m -my_order ${order} -my_output_prt 100 -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_atol 1e-12 -ksp_rtol 1e-12 -snes_monitor -snes_type newtonls -snes_linesearch_type basic -snes_max_it 30 -snes_atol 1e-6 -snes_rtol 1e-8 -ts_type beuler -ts_max_snes_failures 1 -ts_dt 0.5 -ts_final_time 5 -my_load_history load_history3.in -mass_postproc -ts_monitor -less_post_proc -young_modulus 900 -poisson_ratio 0.3 -c 10 -rho_ref 1 -psi_ref 0.0025 -m 3 -n 2 -less_post_proc -equilibrium_stop_rate 0.001 -log_summary | tee log && grep "Time (sec):" log | awk '{print $3}' | tee -a log_time

if [ proc==1 ]
then
    break
fi

done
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' log_time >> log_stand
rm log_time

echo 'FIELD SPLIT'
for var in 0 1 2
do
mpirun -mca oob_tcp_keepalive_time 0 -np ${proc} ./bone_adaptation -my_file out.h5m -my_order ${order} -my_output_prt 100 -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_atol 1e-12 -ksp_rtol 1e-12 -snes_monitor -snes_type newtonls -snes_linesearch_type basic -snes_max_it 30 -snes_atol 1e-6 -snes_rtol 1e-8 -ts_type beuler -ts_max_snes_failures 1 -snes_max_linear_solve_fail 1000 -ts_dt 0.5 -ts_final_time 5 -my_load_history load_history3.in -mass_postproc -ts_monitor -less_post_proc -young_modulus 900 -poisson_ratio 0.3 -c 10 -rho_ref 1 -psi_ref 0.0025 -m 3 -n 2 -less_post_proc -equilibrium_stop_rate 0.001 -log_summary \
-ksp_type gmres -pc_type fieldsplit \
-ksp_atol 1e-8 -ksp_rtol 1e-8 \
-ksp_max_it 1000 \
-fieldsplit_0_ksp_type preonly \
-fieldsplit_0_pc_type lu \
-fieldsplit_0_pc_factor_mat_solver_package mumps \
-fieldsplit_1_ksp_type preonly \
-fieldsplit_1_pc_type lu \
-fieldsplit_1_pc_factor_mat_solver_package mumps \
-pc_fieldsplit_type schur \
-pc_fieldsplit_schur_precondition a11 | tee log && grep "Time (sec):" log | awk '{print $3}' | tee -a log_time

if [ proc==1 ]
then
    break
fi

done
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' log_time >> log_fsplit
rm log_time

echo 'FIELD SPLIT HYPRE'
for var in 0 1 2
do
mpirun -mca oob_tcp_keepalive_time 0 -np ${proc} ./bone_adaptation -my_file out.h5m -my_order ${order} -my_output_prt 100 -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_atol 1e-12 -ksp_rtol 1e-12 -snes_monitor -snes_type newtonls -snes_linesearch_type basic -snes_max_it 30 -snes_atol 1e-6 -snes_rtol 1e-8 -ts_type beuler -ts_max_snes_failures 1 -snes_max_linear_solve_fail 1000 -ts_dt 0.5 -ts_final_time 5 -my_load_history load_history3.in -mass_postproc -ts_monitor -less_post_proc -young_modulus 900 -poisson_ratio 0.3 -c 10 -rho_ref 1 -psi_ref 0.0025 -m 3 -n 2 -less_post_proc -equilibrium_stop_rate 0.001 -log_summary \
-ksp_type gmres -pc_type fieldsplit \
-ksp_atol 1e-8 -ksp_rtol 1e-8 \
-ksp_max_it 1000 \
-fieldsplit_0_ksp_type gmres \
-fieldsplit_0_ksp_max_it 25 \
-fieldsplit_0_pc_type hypre \
-fieldsplit_0_pc_hypre_boomeramg_max_iter 4 \
-fieldsplit_1_ksp_max_it 25 \
-fieldsplit_1_pc_type hypre \
-fieldsplit_1_pc_hypre_boomeramg_max_iter 4 \
-fieldsplit_1_ksp_monitor \
-pc_fieldsplit_type multiplicative | tee log && grep "Time (sec):" log | awk '{print $3}' | tee -a log_time

if [ proc==1 ]
then
    break
fi

done
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' log_time >> log_hypre
rm log_time

done

