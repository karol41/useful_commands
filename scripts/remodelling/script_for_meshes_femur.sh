
#!/bin/bash

order=2



for file in femur*case*.cub
do
FILE=$file

for proc in 4
do
mpirun -np 1 ../tools/mofem_part -my_file $FILE -my_nparts ${proc}


echo 'FIELD SPLIT MUMPS'
mpirun -np 4 ./bone_adaptation \
-my_file out.h5m -my_order 2 -my_output_prt 10 \
-ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps \
-ksp_atol 1e-12 -ksp_rtol 1e-12 -snes_monitor -snes_type newtonls \
-snes_linesearch_type basic \
-snes_max_it 1000 -snes_atol 1e-6 -snes_rtol 1e-8 \
-ts_type beuler -ts_max_snes_failures 1 \
-ts_dt 0.1 -ts_final_time 10 -my_load_history ./examples/load_history2.in \
-mass_postproc -ts_monitor -young_modulus 500 -poisson_ratio 0.2 -c 1 \
-rho_ref 1.2 -psi_ref 0.01 -m 3 -n 2 | tee log
done

mbconvert out_100.h5m ${file%cub}vtk



done
