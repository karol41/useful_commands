
#!/bin/bash

#order=2
c=5
proc=8

for file in bone_test_no_screw2_load0.cub
do

for order in 1 2 3 4 5
do

mpirun -np 1 ../tools/mofem_part -my_file $file -my_nparts ${proc}


echo 'FIELD SPLIT MUMPS'
mpirun -np ${proc} ./bone_adaptation -my_file out.h5m -my_order ${order} -my_output_prt 1 -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_atol 1e-12 -ksp_rtol 1e-12 -snes_monitor -snes_type newtonls -snes_linesearch_type basic -snes_max_it 30 -snes_atol 1e-6 -snes_rtol 1e-8 -ts_type beuler -ts_max_snes_failures -1 -snes_max_linear_solve_fail 1000 -ts_dt 0.5 -ts_final_time 10 -my_load_history load_history3.in -mass_postproc -ts_monitor -young_modulus 900 -poisson_ratio 0.3 -c $c -rho_ref 1 -psi_ref 0.0025 -m 3 -n 2 -equilibrium_stop_rate 0.001 -log_summary \
-ksp_type gmres -pc_type fieldsplit \
-ksp_atol 1e-8 -ksp_rtol 1e-8 \
-ksp_max_it 1000 \
-fieldsplit_0_ksp_type preonly \
-fieldsplit_0_pc_type lu \
-fieldsplit_0_pc_factor_mat_solver_package mumps \
-fieldsplit_1_ksp_type preonly \
-fieldsplit_1_pc_type lu \
-fieldsplit_1_pc_factor_mat_solver_package mumps \
-pc_fieldsplit_type multiplicative | tee log_${file%.cub}_ord${order}



mbconvert out_10.h5m ${file%.cub}_${order}.vtk
done


done
