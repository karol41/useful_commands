#!/bin/bash
  
file=bone_test_no_screw3.cub
#DELETE HIGHER ORDER NODES
rm out_*h5m
proc=2

  if [ -n "$1" ]
   then
    proc=$1
  fi

echo "Proc: ${proc}"

if [ "$2" = 'elastic' ]
then
#	../../tools/mofem_part -my_file out.h5m -my_nparts $proc
	../../basic_finite_elements/elasticity/elasticity -my_file ${file}  \
  -ksp_type gmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor \
  -my_order 2 
  echo "elasticity TEST..."
  exit 0
fi

#CUT THE MESH
#../tools/mesh_cut -my_file out.h5m -surface_side_set 400 -fraction_level 1 -create_side_set 200  -shift 0,0,0 -vol_block_set 1

../../tools/mofem_part -my_file $file -my_nparts ${proc}

if [ "$2" = 'with_map' ]
then
  echo "FILE WITH MAPPED DENSITY"
  mpirun -np 2 ../mass_calculation \
  -meta_image CT_attempt_quadr_full.mhd -my_file $file \
  -param_density 0.00068,0.92 \
  -cube_size 4 \
  -dist_tol .5 \
  -my_order 2 \
  -lambda 0.25 \
  -scale 0.1 \
  -ksp_type cg -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor

  ../../tools/mofem_part -my_file analysis_mesh.h5m -my_nparts ${proc}
fi
#exit 0


order=2

mpirun -np ${proc} ../bone_adaptation -my_file out.h5m -my_order ${order} -my_output_prt 1 \
-ksp_atol 1e-12 -ksp_rtol 1e-12 -snes_monitor -snes_type newtonls -snes_linesearch_type basic -snes_max_it 30 -snes_atol 1e-8 -snes_rtol 1e-8 \
-ts_type beuler  -snes_max_linear_solve_fail 1000 \
-ts_max_snes_failures -1 -analysis_mesh \
-my_load_history load_history2.in -mass_postproc -ts_monitor -less_post_proc \
-b 0 -rho_max 1.8 -rho_min 1.0  \
-ts_dt 0.5 -ts_final_time 1600 \
-ts_adapt_dt_max 50.0 -ts_adapt_dt_min 0.005 \
-ts_adapt_type none -ts_adapt_basic_safety 0.8 \
-ts_adapt_monitor \
-ts_rtol 0.01 -ts_atol 0.01  \
-young_modulus 478 -poisson_ratio 0.2 -c 1 -rho_ref 1 -psi_ref 0.025 -m 3.25 -n 2.25 -less_post_proc -equilibrium_stop_rate 0.01 -log_view \
-ksp_type fgmres -pc_type fieldsplit \
-ksp_atol 1e-8 -ksp_rtol 1e-8 \
-ksp_max_it 1000 \
-fieldsplit_0_ksp_type preonly \
-fieldsplit_0_pc_type lu \
-fieldsplit_0_pc_factor_mat_solver_package mumps \
-fieldsplit_1_ksp_type preonly \
-fieldsplit_1_pc_type lu \
-fieldsplit_1_pc_factor_mat_solver_package mumps \
-pc_fieldsplit_type multiplicative  | tee log
# -fieldsplit_0_ksp_type gmres \
# -fieldsplit_0_ksp_max_it 100 \
# -fieldsplit_0_pc_type bjacobi \
# -fieldsplit_0_sub_pc_type ilu \
# -fieldsplit_0_sub_pc_factor_levels 2 \
# -fieldsplit_1_ksp_type gmres \
# -fieldsplit_1_ksp_max_it 100 \
# -fieldsplit_1_pc_type bjacobi \
# -fieldsplit_1_sub_pc_type ilu \
# -fieldsplit_1_sub_pc_factor_levels 2 \
# -pc_fieldsplit_type multiplicative | tee log && grep "Time (sec):" log | awk '{print $3}' | tee -a log_time3

echo "Done"

