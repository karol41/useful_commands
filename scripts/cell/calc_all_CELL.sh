#!/bin/bash


proc=8;
echo 'RIGID'
for f in *Rigid*dx.dat;
do
  echo $f
  echo ${f%dx.dat}dy.dat
  ../map_disp_prism -my_thinckness 0.01 -my_data_y ${f} -my_data_x ${f%dx.dat}dy.dat -my_file ../mesh_cell_force_map.cub -ksp_type cg -pc_type lu \
  -pc_factor_mat_solver_package mumps -ksp_monitor -lambda 1 -my_order 3 -scale 3.3 -cube_size 1 -my_nparts ${proc}

  mpirun -np ${proc} ../cell_forces -my_file analysis_mesh.h5m -my_order 1 -my_order_force 2 -my_max_post_proc_ref_level 1 -my_block_config block_config_rigid.in \
  -ksp_type fgmres -ksp_monitor -fieldsplit_1_ksp_type fgmres -fieldsplit_1_pc_type lu -fieldsplit_1_pc_factor_mat_solver_package mumps -fieldsplit_1_ksp_max_it 25 -fieldsplit_1_ksp_monitor  \
  -fieldsplit_0_ksp_type gmres -fieldsplit_0_ksp_max_it 25 -fieldsplit_0_fieldsplit_0_ksp_type preonly -fieldsplit_0_fieldsplit_0_pc_type lu \
  -fieldsplit_0_fieldsplit_0_pc_factor_mat_solver_package mumps -fieldsplit_0_fieldsplit_1_ksp_type preonly -fieldsplit_0_fieldsplit_1_pc_type lu \
  -fieldsplit_0_fieldsplit_1_pc_factor_mat_solver_package mumps -ksp_atol 1e-6 -ksp_rtol 0 -my_eps_u 1e-4 -my_curl 1 | tee log


  mbconvert out_tractions.h5m ${f%dx.dat}_tractions.vtk ;
  mbconvert out.h5m ${f%dx.dat}.vtk ;
  mbconvert out_disp.h5m ${f%dx.dat}_disp.vtk ;
done



for f in *Medium*dx.dat;
do
  echo $f
  echo ${f%dx.dat}dy.dat
  ../map_disp_prism -my_thinckness 0.01 -my_data_y ${f} -my_data_x ${f%dx.dat}dy.dat -my_file ../mesh_cell_force_map.cub -ksp_type cg -pc_type lu \
  -pc_factor_mat_solver_package mumps -ksp_monitor -lambda 1 -my_order 3 -scale 3.3 -cube_size 1 -my_nparts ${proc}

  mpirun -np ${proc} ../cell_forces -my_file analysis_mesh.h5m -my_order 1 -my_order_force 2 -my_max_post_proc_ref_level 1 -my_block_config block_config_medium.in \
  -ksp_type fgmres -ksp_monitor -fieldsplit_1_ksp_type fgmres -fieldsplit_1_pc_type lu -fieldsplit_1_pc_factor_mat_solver_package mumps -fieldsplit_1_ksp_max_it 25 -fieldsplit_1_ksp_monitor  \
  -fieldsplit_0_ksp_type gmres -fieldsplit_0_ksp_max_it 25 -fieldsplit_0_fieldsplit_0_ksp_type preonly -fieldsplit_0_fieldsplit_0_pc_type lu \
  -fieldsplit_0_fieldsplit_0_pc_factor_mat_solver_package mumps -fieldsplit_0_fieldsplit_1_ksp_type preonly -fieldsplit_0_fieldsplit_1_pc_type lu \
  -fieldsplit_0_fieldsplit_1_pc_factor_mat_solver_package mumps -ksp_atol 1e-6 -ksp_rtol 0 -my_eps_u 1e-4 -my_curl 1 | tee log


  mbconvert out_tractions.h5m ${f%dx.dat}_tractions.vtk ;
  mbconvert out.h5m ${f%dx.dat}.vtk ;
  mbconvert out_disp.h5m ${f%dx.dat}_disp.vtk ;
done

echo 'All finished succesfully!'


