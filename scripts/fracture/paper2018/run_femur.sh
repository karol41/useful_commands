#!/bin/bash

CURRENT_DIR=$PWD
echo "Script location: ${CURRENT_DIR}"

# cd $MOFEM_INSTALL_DIR/users_modules/fracture_mechanics
proc=$2
if [ "$1" = 1 ]
then
 if [ -f "analysis_mesh_vertex.h5m" ]
 then
#Mapping with bone remodelling
mpirun -mca oob_tcp_keepalive_time 0 -np 2 ../bone_remodelling/mass_calculation \
-meta_image ../bone_remodelling/proximal_femur_map2.mhd -my_file femur_abaqus.cub \
-param_density 0.0003,1 -cube_size 3 -dist_tol 0.5 -my_order 2 -lambda 0 -scale 1 \
-ksp_type cg -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor 

#field to vertices the mesh
../tools/delete_ho_nodes -my_file analysis_mesh.h5m
../tools/field_to_vertices -my_file out.h5m
mv out.h5m analysis_mesh_vertex.h5m
fi
#DELETE HIGHER ORDER NODES
../tools/delete_ho_nodes -my_file femur_abaqus_for_CUT.cub

#CUT THE MESH
../tools/mesh_cut -my_file out.h5m -surface_side_set 400 -fraction_level 1 -create_side_set 200  -shift 0,0,0 -vol_block_set 1
mv out.h5m femur_abaqus_for_CUT.h5m 
# scp femur_abaqus_for_CUT.h5m $server/my_build/users_modules/fracture_mechanics/ 
# ../tools/split_sideset -my_file out.h5m -side_sets 200
# ../basic_finite_elements/elasticity/elasticity -my_file out.h5m  -ksp_type gmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -my_order 2
# ../basic_finite_elements/elasticity/elasticity -my_file femur_abaqus_for_CUT.h5m  -ksp_type gmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -my_order 2


proc=2
 #crack propagation
  ../tools/mofem_part -my_file femur_abaqus_for_CUT.h5m -my_nparts $proc
    mpirun -np $proc ./crack_propagation \
    -my_file out.h5m \
    -snes_monitor -snes_atol 1e-8 -snes_rtol 0 -snes_converged_reason -snes_linesearch_monitor  -ksp_monitor -ksp_type fgmres \
    -my_max_post_proc_ref_level 0 -ksp_atol 1e-12 -ksp_rtol 0 -my_order 2 -mofem_mg_verbose 1 -mofem_mg_coarse_order 1 \
    -mofem_mg_levels 2 -mg_coarse_ksp_type preonly -mg_coarse_pc_type lu -mg_coarse_pc_factor_mat_solver_package mumps \
    -pc_mg_smoothup 10 -pc_mg_smoothdown 10 -pc_mg_galerkin true -pc_mg_type multiplicative -pc_type lu -pc_factor_mat_solver_package mumps \
    -my_ref 0 -my_geom_order 1 -my_ref_order 0 -material BONEHOOKE -my_add_singularity 1 -my_n_bone 2 -my_rho0 1 -nb_load_steps 1 -load_scale 1 \
    -mwls_dm 1.4 -mwls_number_of_base_functions 10 -my_mwls_approx_file analysis_mesh_vertex.h5m -my_density_block 1 -my_propagate_crack 0 -cut_mesh 0
    # -cut_mesh 1 -cut_surface_side_set 400 -edges_block_set 22 -vertex_block_set 32 -fraction_level 2

#output
mbconvert out_spatial0.h5m out.vtk

elif [ "$1" = 2 ]
then

mpirun -np 2 ../bone_remodelling/mass_calculation \
-meta_image ../bone_remodelling/cube_for_test.mhd  -my_file ../bone_remodelling/3tet_mesh.cub \
-param_density 1,0 -cube_size 3 -dist_tol 0 -my_order 2 -lambda 0 -scale .1 \
-ksp_type cg -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor 


#RHO to vertices
../tools/field_to_vertices -my_file analysis_mesh.h5m
mv out.h5m analysis_mesh_vertex.h5m


#cut the mesh2
./mesh_cut -my_file ../mwls_approx/3tetmesh_4cut.cub  -edges_block_set 200 -vertex_block_set 300 -surface_side_set 400 -vol_block_set 1 -fraction_level 3 -create_side_set 200  -shift .1,.1,0 
#../basic_finite_elements/elasticity/elasticity -my_file out.h5m  -ksp_type gmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -my_order 2 

# RHO to vertices
../tools/field_to_vertices -my_file analysis_mesh.h5m
mv out.h5m analysis_mesh_vertex.h5m

#cut the mesh
../tools/mesh_cut -my_file femur_abaqus_for_CUT.cub -surface_side_set 400 -fraction_level 2 -create_side_set 200 -vol_block_set 1 -shift .1,.1,0 
# ../tools/split_sideset -my_file out.h5m -side_sets 200
# ./split_sideset -my_file out.h5m -side_sets 200
#../basic_finite_elements/elasticity/elasticity -my_file out.h5m  -ksp_type gmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -my_order 2 

mv out.h5m mesh_with_cut_out.h5m

#separate the cut with elastic problem 

#MWLS mapping
#./mwls_approx -my_file 3tet_mesh.h5m -my_mwls_approx_file out_mesh.vtk \
# -mwls_dm .9 -mwls_number_of_base_functions 1 


 #crack propagation
  ../tools/mofem_part -my_file femur_abaqus_for_CUT.h5m -my_nparts 2
    mpirun -np 2 ./crack_propagation \
    -my_file out.h5m \
    -snes_monitor -snes_atol 1e-8 -snes_rtol 0 -snes_converged_reason -snes_linesearch_monitor  -ksp_monitor -ksp_type fgmres \
    -my_max_post_proc_ref_level 0 -ksp_atol 1e-12 -ksp_rtol 0 -my_order 2 -mofem_mg_verbose 1 -mofem_mg_coarse_order 1 \
    -mofem_mg_levels 2 -mg_coarse_ksp_type preonly -mg_coarse_pc_type lu -mg_coarse_pc_factor_mat_solver_package mumps \
    -pc_mg_smoothup 10 -pc_mg_smoothdown 10 -pc_mg_galerkin true -pc_mg_type multiplicative -pc_type lu -pc_factor_mat_solver_package mumps \
    -my_ref 0 -my_geom_order 1 -my_ref_order 0 -material BONEHOOKE -my_add_singularity 1 -my_n_bone 1 -my_rho0 1 -nb_load_steps 1 -load_scale 1 \
    -mwls_dm 1.4 -mwls_number_of_base_functions 10 -my_mwls_approx_file analysis_mesh_vertex.h5m -my_density_block 1 \
    # -cut_mesh 1 -cut_surface_side_set 400 -edges_block_set 22 -vertex_block_set 32 -fraction_level 2


elif [ "$1" = 3 ]
then
    ../../tools/delete_ho_nodes -my_file femur_abaqus_for_CUT.cub 
    
    ../../tools/mofem_part -my_file femur_abaqus_for_CUT.h5m -my_nparts $proc
    mpirun -np $proc ../crack_propagation -my_file out.h5m | tee -a log
  
else
 echo "Provide correct number of the case."

fi