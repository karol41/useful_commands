#!/bin/bash

rsync -avz -r *.* --exclude '*.vtk' --exclude '*.h5m'  --exclude '*.petsc'  karol@rdb-srv1.eng.gla.ac.uk:~/my_build/users_modules/fracture_mechanics/paper_2018/
