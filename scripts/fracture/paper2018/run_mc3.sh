#!/bin/bash

CURRENT_DIR=$PWD
echo "Script location: ${CURRENT_DIR}"

# cd $MOFEM_INSTALL_DIR/users_modules/fracture_mechanics

#PARAMETERS PARAMETERS PARAMETERS PARAMETERS PARAMETERS PARAMETERS PARAMETERS 
proc=$2
load_scale=2


if [ "$1" = 1 ]
then

rm -f max_log.txt
#echo 'file_number max_g1 energy' > max_log.txt
for file in analysis_mesh_*h5m
do

 data_mesh=${file%.h5m}_vertex.h5m
 data_mesh=${data_mesh//"mesh_"}

 echo "File $data_mesh processing..."
 if [ ! -f $data_mesh ]
 then
    #Mapping with bone remodelling

    #field to vertices the meshma
   
    ../../tools/delete_ho_nodes -my_file $file
    ../../tools/field_to_vertices -my_file out.h5m
    mv out.h5m $data_mesh
    mbconvert $data_mesh ${data_mesh%.h5m}.vtk
fi
#DELETE HIGHER ORDER NODES
# ../../tools/delete_ho_nodes -my_file bone_mc3_crack_dens.cub
../../tools/delete_ho_nodes -my_file bone_mc3_crack_elastic.cub
cp out.h5m calc_mesh.h5m
#CUT THE MESH
 #../../tools/mesh_cut -my_file out.h5m -surface_side_set 400 -fraction_level 2 -create_side_set 200 -shift 0,0,0 -vol_block_set 1 -edges_block_set 200 -vertex_block_set 300 -tol_cut 2e-2 -tol_cut_close 1e-1 -tol_trim 1e-1 -tol_trim_close 1e-2
#mv out.h5m bone_mc3_crack_dens.h5m 
# scp bone_mc3_crack_dens.h5m $server/my_build/users_modules/fracture_mechanics/ 
# ../../tools/split_sideset -my_file out.h5m -side_sets 200
# ../../basic_finite_elements/elasticity/elasticity -my_file out.h5m  -ksp_type gmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -my_order 2
# ../../basic_finite_elements/elasticity/elasticity -my_file bone_mc3_crack_dens.h5m  -ksp_type gmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_monitor -my_order 2

 #crack propagation
  ../../tools/mofem_part -my_file calc_mesh.h5m -my_nparts $proc
    mpirun -np $proc ../crack_propagation \
    -my_file out.h5m \
    -snes_monitor -snes_atol 1e-8 -snes_rtol 0 -snes_converged_reason -snes_linesearch_monitor  -ksp_monitor -ksp_type fgmres \
    -my_max_post_proc_ref_level 0 -ksp_atol 1e-12 -ksp_rtol 0 -my_order 2 -mofem_mg_verbose 1 -mofem_mg_coarse_order 1 \
    -mofem_mg_levels 2 -mg_coarse_ksp_type preonly -mg_coarse_pc_type lu -mg_coarse_pc_factor_mat_solver_package mumps \
    -pc_mg_smoothup 10 -pc_mg_smoothdown 10 -pc_mg_galerkin true -pc_mg_type multiplicative -pc_type lu -pc_factor_mat_solver_package mumps \
    -my_ref 0 -my_geom_order 1 -my_ref_order 1 -material BONEHOOKE -my_add_singularity 1 -my_n_bone 2 -my_rho0 1 -nb_load_steps 1 -load_scale $load_scale \
    -mwls_dm 1.4 -mwls_number_of_base_functions 10 -my_mwls_approx_file $data_mesh -my_density_block 1 -my_propagate_crack 0 -cut_mesh 1 | tee log

#save VTK file with gc points 
file_number=${file#'analysis_mesh_'}
file_number=${file_number%.h5m}
#nb_points=$(awk 'END{print NR}' log) 
nb_points=$(grep -w 'griffith force at ent' -c log)
printf "# vtk DataFile Version 4.2\n" > griff_pts_${file_number}.vtk
printf "vtk output\nASCII\nDATASET UNSTRUCTURED_GRID \nPOINTS ${nb_points} float \n" >> griff_pts_${file_number}.vtk
grep "griffith force at ent" log | awk '{print $7,$8,$9}' >> griff_pts_${file_number}.vtk
printf "\nPOINT_DATA ${nb_points} \n" >> griff_pts_${file_number}.vtk
printf "VECTORS GRIFF_FORCE double \n" >> griff_pts_${file_number}.vtk
grep "griffith force at ent" log | awk '{print $11,$18,$23}' >> griff_pts_${file_number}.vtk

#output file
mbconvert out_spatial_0.h5m out_spatial_${file_number}.vtk
cp out_crack_front_0.vtk out_crack_front_${file_number}.vtk


#save log file with file number, max gc and elastic energy
energy=$(grep "Elastic energy" log | awk '{print $3}')
echo $file_number $(grep 'max' log | awk '{print $4}')  $energy >>  max_log.txt


done

sort -n max_log.txt > tmp
mv tmp max_log.txt
(echo "file_number max_g1 energy" && cat max_log.txt) > filename1 && mv filename1 max_log.txt
echo "DONE"

elif [ "$1" = 2 ]
then

echo "mwls mwls mwls mwls mwls mwls mwls mwls mwls mwls mwls mwls mwls mwls mwls mwls"

../../tools/delete_ho_nodes -my_file bone_mc3_crack_elastic.cub
mv out.h5m bone_mc3_crack_elastic.h5m

#MWLS mapping
../../mwls_approx/mwls_approx -my_file bone_mc3_crack_elastic.h5m -my_mwls_approx_file analysis_mc3_vertex_mc3.h5m \
 -mwls_dm 1.8 -mwls_number_of_base_functions 10 


mbconvert out_mwls.h5m out_mwls.vtk && open out_mwls.vtk 


elif [ "$1" = 3 ]
then
    file=bone_mc3_crack_elastic_FINE.cub
    rm -f log
    load_scale=1
    ../../tools/delete_ho_nodes -my_file $file
    mv out.h5m bone_mc3_crack_elastic.h5m
    
    ../../tools/mofem_part -my_file bone_mc3_crack_elastic.h5m -my_nparts $proc

    mpirun -np $proc ../crack_propagation -my_file out.h5m -load_scale $load_scale | tee -a log

    while [ ! ${PIPESTATUS[0]} -eq 0  ]
    do
     echo "### RESTART ANALSIS ### MUMPS ERROR" | tee -a log
     restart_file=$(ls -t1 restart_*h5m |  head -n 1)   
     mpirun -np $proc ../crack_propagation -my_file $restart_file | tee -a log
    done
  
else
 echo "Provide correct number of the case."

fi
