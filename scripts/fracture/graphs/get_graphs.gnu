# This gnuplot script generates .ps graphs for displacement - force and crack area - energy relations
set terminal postscript color   # eps enhanced "Verdana" 12
# Plot displacement - force relation
#set size 1,1
set output "graph_displacement_force.ps"
set xlabel "Displacement (m)"
set ylabel "Load factor"
unset key
set key right # box
#plot [0.:] [0.:]   "displacement_force.txt" u 2:3 title "Displacement vs. Force" w lp pt 7 ps 1.5 lc rgb "#E81C82" lw 5 dt 1  pointinterval 1
plot "displacement_force_crackArea_energy.txt" u 2:3 title "Displacement vs. Force" w lp pt 7 ps 1.5 lc rgb "blue" lw 5 dt 1  pointinterval 1
# Plot crack area - energy relation
set output "graph_crackArea_energy.ps"
set xlabel "Crack area (m2)"
set ylabel "Elastic energy (MJ)"
unset key
set key right # box
# Plotting energy in unit of MJ. Ignoring the first row with zeros
plot "displacement_force_crackArea_energy.txt" every ::1 u 4:($5*1) title "Crack area vs. Elastic energy" w lp pt 7 ps 1.5 lc rgb "blue" lw 5 dt 1  pointinterval 1     
#plot [0.:] [0.:]   "crack_area_energy_kJ.txt" u 2:3 title "Crack area vs. Elastic energy" w lp pt 7 ps 1.5 lc rgb "#E81C82" lw 5 dt 1  pointinterval 1
