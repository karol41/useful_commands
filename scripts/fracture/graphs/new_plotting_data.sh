#!/bin/bash
# This script generates data file and plots graphs for displacement - force and crack area - elastic energy relations in MOFEM fracture module
# Files should be in the same directory: log, get_graphs.gnu

# Create raw data file of displacement - force - crack area - energy from log file (NOT including 'Not Converged Propgation')
grep -E "(Propgation step|F_lambda2|Crack surface area)" log | sed 'N;s/\nCrack//;P;D'| grep -v "Not Converged Propgation" | awk 'BEGIN  {print "-1 0 0 0 0"} /F_lambda2 / { lambda = $14 } /Propgation step/ { F=lambda;  print $3 "\t" 2*$9/F "\t" F "\t" $13 "\t" $9}'| tee data_raw.dat

# Keep updated data only and write it to txt file
tail -r data_raw.dat | sort -u -n | tee displacement_force_crackArea_energy.txt

# Format processed data file
printf "%14s  %14s  %14s  %14s  %14s\n" $(cat displacement_force_crackArea_energy.txt) > temp_file; mv temp_file displacement_force_crackArea_energy.txt            # Format columns
cat displacement_force_crackArea_energy.txt         # Print out final data file

# Make graphs using gnuplot. Note: ignore the first row of zeros when plotting crack area - energy relation
gnuplot -e "l 'get_graphs.gnu'; q"      # Activate gnuplot; Run gnuplot script; Quit gnuplot

# Convert graphs on .ps to .pdf
for i in `ls *.ps`; do
    ps2pdf $i;
    rm $i;
    # echo 'Converted $i into pdf, deleted original file.';
done

# Remove unnecessary files
rm -f *.dat

# Open pdf graphs
open graph_*.pdf
