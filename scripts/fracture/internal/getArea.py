# trace generated using paraview version 5.5.0
#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

input_filename_root = 'out_crack_surface_'
output_filename_root = 'default'
# = '/Users/ignathanas/jobs/StressIntensity/TestScripts/'

import os
directory = os.path.dirname(os.path.abspath(__file__)) + "/"


out_filename = directory + output_filename_root + ".csv"
open(out_filename, 'w').close()



for i in os.listdir(directory):
    files =  [i for i in os.listdir(directory) if os.path.isfile(os.path.join(directory,i)) and \
         'out_crack_surface_' in i]
    

import re

def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split('(\d+)', text) ]


files.sort(key=natural_keys)
x = 0   
for filename in files:
    print(filename)
    out_filename = directory + output_filename_root +".csv"

    if x == 0:
        fout = open(out_filename, "a")
        open(out_filename, 'w').close()
        fout.write("0.\n")
        fout.close()

    helpFile = directory + "help.csv"

    help_file = directory + filename 

    out_crack_surface_0vtk = LegacyVTKReader(FileNames=[help_file])
    integrateVariables1 = IntegrateVariables(Input=out_crack_surface_0vtk)
    
    SaveData(helpFile, proxy=integrateVariables1, FieldAssociation="Cells")
    
    with open(helpFile, "r") as f:
        lines = f.readlines()

    fout = open(out_filename, "a")
    
   
    pivot_values = lines[1].split(',')
    if len(pivot_values) == 10:
        fout.write(pivot_values[8])
    elif len(pivot_values) == 9:
        fout.write(pivot_values[7])
    elif len(pivot_values) == 2:
        fout.write(pivot_values[0])
    else:
        fout.write("Error") 
    fout.write("\n")
    fout.close()
    x+=1
    os.remove(helpFile)
