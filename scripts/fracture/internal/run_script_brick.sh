#!/bin/bash
proc=$1
mesh_file="brick_file.cub"
#
# 1st param PROC
# 2nd param MESH_FILES
# 3rd param MWLS_FILES
#
# example:
# ./run_script_brick.sh 8 "brick_file_*bc1.cub" "out_for_mwls_*m.h5m"
#
# if [ -z "$2" ]; then
# 	echo "No argument supplied"
# else
# 	mesh_file=$2
# fi

# FOR BUCKETHEAD MPI
# $HOME/um_view/bin/mpirun
# 0 	1
# -2 3
#  2 1
# -0 -1

# REMEMBER TO CHANGE MPIRUN

# echo "0 *** scale *** coeff"
#test
if [ "$1" = 0 ]; then
	# for file in out_for_mwls_*m.h5m; do
	# HERE YOU CAN INCREASE THE SCALE (GRADIENT)
	for scale in $2; do
		# echo $file
		file="out_for_mwls.h5m"
		../create_mwls_stress_mesh -my_file $mesh_file -my_scale $scale -my_coeff $3
		../mwls -my_file $mesh_file -my_residual_stress_block 1 \
			-my_mwls_approx_file $file -mwls_dm 1 -mwls_number_of_base_functions 10
		mbconvert out_mwls.h5m out_mwls_$scale.vtk
		open out_mwls_$scale.vtk
		mv out_for_mwls.h5m out_for_mwls${scale}.h5m
	done
else

	# for mesh_file in brick_file_*bc1.cub; do
	for mesh_file in $2; do
		# for file_mwls in out_for_mwls*.h5m; do
		for file_mwls in $3; do
			for order in 1; do
				int=0
				folder=DR_${file_mwls%.h5m}_dir_${mesh_file}_${order}
				rm -rf $folder
				mkdir $folder
				cp $mesh_file $file_mwls param_file.petsc $folder
				cp getArea.py gen.sh $folder
				cd $folder
				mpirun -np $proc ../../crack_propagation -my_file $mesh_file \
					-my_residual_stress_block 1 -my_mwls_approx_file $file_mwls -mwls_dm 1 -mwls_number_of_base_functions 10 \
					-my_ref 1 -my_order $order -my_ref_order 2 -my_propagate_crack 1 -nb_cut_steps 25 \
					2>&1 | tee log

				while [[ ! ${PIPESTATUS[0]} -eq 0 ]] && [[ $int -lt 3 ]]; do
					echo "### RESTART ANALSIS ### MUMPS ERROR:" $int | tee -a log
					restart_file=$(ls -t1 restart_*h5m | head -n 1)
					file_no=${restart_file#"restart_"}
					file_no=${file_no%".h5m"}
					rand_no=$(python -c "import random; print random.randint(0,2)")
					file_no=$((file_no - rand_no))
					mpirun -np $proc ../../crack_propagation -my_file restart_$file_no.h5m \
						-my_residual_stress_block 1 -my_mwls_approx_file $file_mwls -mwls_dm 1 -mwls_number_of_base_functions 10 \
						-my_ref 1 -my_order $order -my_ref_order 2 -my_propagate_crack 1 -nb_cut_steps 25 \
						2>&1 | tee -a log
					# screen -dm mpirun -np 6 ../../crack_propagation -my_file restart_15.h5m \
					# -my_residual_stress_block 1 -my_mwls_approx_file ./out_for_mwls*.h5m -mwls_dm 1 -mwls_number_of_base_functions 10 \
					# -my_ref 1 -my_order 2 -my_ref_order 2 -my_propagate_crack 1 -nb_cut_steps 25 \
					# 2>&1 | tee -a log
					int=$((int + 1))
					# mpirun -np $proc ../crack_propagation -my_file $restart_file | tee -a log
				done
				# mv *.vtk out_spatial*.h5m restart*.h5m $folder
				grep -E "(Propgation step|F_lambda2)" log | grep -v "Not Converged Propgation" | awk 'BEGIN {print "-1 0 0"} /F_lambda2 / { lambda = $14 } /Propgation step/ { F=lambda;  print $3,2*$9/F,F }' | tee gp_3nd
				# mv gp_3nd log $folder
				cd ..
			done
		done
		# restart_file=$(ls -t1 restart_*h5m | head -n 1) && mpirun -np 8 ../../crack_propagation -my_file $restart_file | tee -a log
	done
fi
