#!/bin/bash

#1)
grep -E "(Propgation step|F_lambda2)" log | awk 'BEGIN {print "-1 0 0"} /F_lambda2 / { lambda = $14 } /Propgation step/ { F=lambda;  print $3,2*$9/F,F }' | tee intermediate;
#2)
/Applications/ParaView-5.5.0.app/Contents/bin/pvpython getArea.py;

#3) combine
paste -d " " intermediate default.csv > helpfile;

grep -v "Propgation" helpfile | tee input.dat;

rm intermediate
rm helpfile

