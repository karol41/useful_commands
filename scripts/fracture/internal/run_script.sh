#!/bin/bash

proc=$1

#test
if [ "$1" = 0 ]; then
	for file in out_for_mwls_*m.h5m; do
  echo $file
		../create_mwls_stress_mesh -my_file test_internal_stress_dense.cub
		../mwls -my_file test_internal_stress_dense.cub -my_residual_stress_block 1 \
    -my_mwls_approx_file $file -mwls_dm 1 -mwls_number_of_base_functions 10
		mbconvert out_mwls.h5m out_mwls.vtk && open out_mwls.vtk
	done
else

  mwls_file="out_for_mwls.h5m"
  mwls_file="out_for_mwls_Xup.h5m"
	# for file in out_for_mwls_*5hm; do
	rm log
	mpirun -np $proc ../crack_propagation -my_file test_internal_stress_dense.cub \
		-my_residual_stress_block 1 -my_mwls_approx_file $mwls_file -mwls_dm 1 -mwls_number_of_base_functions 10 \
    -my_ref 0 2>&1 | tee log
	# done

	grep -E "(Propgation step|F_lambda2)" log | grep -v "Not Converged Propgation" | awk 'BEGIN {print "-1 0 0"} /F_lambda2 / { lambda = $14 } /Propgation step/ { F=lambda;  print $3,2*$9/F,F }' | tee gp_3nd
fi
