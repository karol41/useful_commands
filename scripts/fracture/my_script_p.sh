#!/bin/bash

proc=4

for order in 1 2 3 4 5
do

../tools/mofem_part -my_file horizontal_crack.cub -my_nparts $proc
for ((ref=0; ref <=7-$order; ref++ ))
do

echo New calculations. Order: $order Ref: $ref

mpirun -np $proc ./crack_propagation -my_file out.h5m  -snes_monitor -snes_atol 1e-8 -snes_rtol 0 -snes_converged_reason -snes_linesearch_monitor  -ksp_monitor -ksp_type fgmres -my_max_post_proc_ref_level 0 -ksp_atol 1e-12 -ksp_rtol 0 -my_order $order -mofem_mg_verbose 1 -mofem_mg_coarse_order 1 -mofem_mg_levels $order -mg_coarse_ksp_type preonly -mg_coarse_pc_type lu -mg_coarse_pc_factor_mat_solver_package mumps -pc_mg_smoothup 10 -pc_mg_smoothdown 10 -pc_mg_galerkin true -pc_mg_type multiplicative -pc_type lu -pc_factor_mat_solver_package mumps -my_ref 0 -my_geom_order 1 -my_ref_order $ref -material HOOKE -nb_load_steps 1 -load_scale 1 -log_summary  2>&1 | tee log0


#stress intensity factor
#grep "griffith force at ent" log0 | awk 'BEGIN { gc = 0 } { print $11; gc += $11 } END {printf "%d %6.4e %4.3f\n",NR,gc/NR,sqrt(1000*gc/NR)} '


#stress intensity factor
grep "griffith force at ent" log0 | awk 'BEGIN { gc = 0 } { gc += $11 } END {printf "%d %6.4e %4.3f\n",NR,gc/NR,sqrt(((sqrt(1000*gc/NR)/1.949936442)-1)^2)*100} ' | awk '{print $3}' | tee log_err 
          
#dofs    
grep "name ELASTIC Nb. local dof" log0 | grep "rank = 0" | grep "row" | awk '{print $25}' | tee log_dof
         
#time   
grep "Time (sec):" log0 | awk '{print $3}' | tee log_time
             
#energ
grep "Elastic energy" log0 | awk '{print $3}' | tee log_energ 
    
echo $order $ref "$(<log_dof)" "$(<log_err)" "$(<log_energ)" "$(<log_time)" | tee -a log_result_order_${order}

mbconvert out_spatial_0.h5m out_spatial_0_${order}_${ref}.vtk 
    
done
done
