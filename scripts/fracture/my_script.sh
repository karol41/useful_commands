#!/bin/bash




if [ "$1" = 1 ]
then

	proc=4
	file=plate_with_single_crack.cub

		if [ -n "$2" ]
		 then
		  echo "File name: $2"
		  file=$2
	  fi

rm log_result_${file}

		if [ -n "$3" ]
		 then
			echo "number of processors: $3"
			proc=$3
		fi

/home/karol/users_modules/tools/mofem_part -my_file $file -my_nparts $proc


for order in 1 2 3 4 5
do

for ((ref=0; ref <=7-$order; ref++ ))
do

echo New calculations. Order: $order Ref: $ref

mpirun -np $proc /home/karol/users_modules/fracture_mechanics/crack_propagation -my_file out.h5m  -snes_monitor -snes_atol 1e-8 -snes_rtol 0 -snes_converged_reason -snes_linesearch_monitor  -ksp_monitor -ksp_type fgmres -my_max_post_proc_ref_level 0 -ksp_atol 1e-12 -ksp_rtol 0 -my_order $order -mofem_mg_verbose 1 -mofem_mg_coarse_order 1 -mofem_mg_levels $order -mg_coarse_ksp_type preonly -mg_coarse_pc_type lu -mg_coarse_pc_factor_mat_solver_package mumps -pc_mg_smoothup 10 -pc_mg_smoothdown 10 -pc_mg_galerkin true -pc_mg_type multiplicative -pc_type lu -pc_factor_mat_solver_package mumps -my_ref 0 -my_geom_order 1 -my_ref_order $ref -material HOOKE -nb_load_steps 1 -load_scale 1 -log_summary -se_number_of_refinment_levels 4 2>&1 | tee log0_${file}

#stress intensity factor
grep "griffith force at ent" log0_${file} | awk 'BEGIN { gc = 0 } { gc += $11 } END {printf "%d %6.4e %4.3f\n",NR,gc/NR,((sqrt(1000*gc/NR)/1.949936442)-1)*100} ' | awk '{print $3}' | tee log_err_${file}

#dofs
grep "name ELASTIC Nb. local dof" log0_${file} | grep "rank = 0" | grep "row" | awk '{print $25}' | tee log_dof_${file}

#time
grep "Time (sec):" log0_${file} | awk '{print $3}' | tee log_time_${file}

#energ
grep "Elastic energy" log0_${file} | awk '{print $3}' | tee log_energ_${file}

echo $order $ref "$(<log_dof_${file})" "$(<log_err_${file})" "$(<log_energ_${file})" "$(<log_time_${file})" | tee -a log_result_${file}

mbconvert out_spatial_0.h5m out_spatial_0_${ref}_${order}.vtk

done
done

rm log0_${file}
rm log_energ_${file}
rm log_dof_${file}
rm log_err_${file}

elif [ "$1" = 2 ]
then

echo Argument is equal to ${1}

rm log_result


proc=2
order=2
ref=5
for file in plate_with_single_crack200.cub plate_with_single_crack198.cub plate_with_single_crack196.cub
do
../tools/mofem_part -my_file $file -my_nparts $proc
file_name=${file%.cub}

echo File: $file | tee -a log_result

mpirun -np $proc /home/karol/users_modules/fracture_mechanics/crack_propagation -my_file out.h5m  -snes_monitor -snes_atol 1e-8 -snes_rtol 0 -snes_converged_reason -snes_linesearch_monitor  -ksp_monitor -ksp_type fgmres -my_max_post_proc_ref_level 0 -ksp_atol 1e-12 -ksp_rtol 0 -my_order $order -mofem_mg_verbose 1 -mofem_mg_coarse_order 1 -mofem_mg_levels $order -mg_coarse_ksp_type preonly -mg_coarse_pc_type lu -mg_coarse_pc_factor_mat_solver_package mumps -pc_mg_smoothup 10 -pc_mg_smoothdown 10 -pc_mg_galerkin true -pc_mg_type multiplicative -pc_type lu -pc_factor_mat_solver_package mumps -my_ref 0 -my_geom_order 1 -my_ref_order $ref -material $2 -my_n_bone 1 -my_rho0 1 -nb_load_steps 1 -load_scale 1 -log_summary  2>&1 | tee log_${file_name}

#stress intensity factor
grep "griffith force at ent" log_${file_name} | awk '{print $11}' | tee log_err

#dofs
#grep "name ELASTIC Nb. local dof" log0 | grep "rank = 0" | grep "row" | awk '{print $25}' | tee log_dof

#time
#grep "Time (sec):" log0 | awk '{print $3}' | tee log_time

crack_length=${file#"plate_with_single_crack"}
crack_length=${crack_length%.cub}
crack_length=${crack_length#0}

#energ
grep "Elastic energy" log_${file_name} | awk '{print $3}' | tee log_energ

echo $order $ref $crack_length $'\n' energy "$(<log_energ)"  $'\n' "$(<log_err)" | tee -a log_result

mbconvert out_spatial_0.h5m out_spatial_0_${file_name}.vtk
done

elif [ "$1" = 3 ]
then

echo "Argument 3 (not implemented yet)"
else
echo "Provide correct number of the case."

fi
