# gnuplot
# l 'filename.in'
# exit
# open 'output.eps'
#

set terminal postscript eps dashed enhanced "Helvetica" 50 size 20,14

set output "output.eps"

#set title ""

#Define axis titles
set xlabel "Time "
set ylabel "K_I Error (%)"

#Define range set xrange or set yrange 
#set xrange [1000:150000]
set yrange [0.6:11]

#set logscale x
set logscale y
set format x "%1.0f"

#Legend position and alignment of the legend labels 
set key top right
set key spacing 3 
#unset key
set grid back lc rgb "grey" linewidth 1
set grid mxtics mytics ls 101 lc rgb "grey" linewidth 5

#Define linestyle
#set style line 1 lt 1

#Define x and y ranges
plot [:] [:] \
'log_result_order_1' u ($6):($4) w linespoints lt 1 pt 7 ps 5 linewidth 5 lc rgb "blue" title "1st order, p 1 to 6",\
'log_result_order_2' u ($6):($4) w linespoints lt 1 pt 7 ps 5 linewidth 5 lc rgb "red" title "2nd order, p 0 to 5",\
'log_result_order_3' u ($6):($4) w linespoints lt 1 pt 7 ps 5 linewidth 5 lc rgb "green" title "3rd order, p 0 to 4",\
'log_result_order_4' u ($6):($4) w linespoints lt 1 pt 7 ps 5 linewidth 5 lc rgb "purple" title "4th order, p 0 to 3",\
'log_result_order_5' u ($6):($4) w linespoints lt 1 pt 7 ps 5 linewidth 5 lc rgb "dark-blue" title "5th order, p 0 to 2"

#open 'output.eps'
