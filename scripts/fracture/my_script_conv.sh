#!/bin/bash


proc=4

if [ "$1" = 1 ]
then

for order in 1 2 3 4 5
do

file=plate_with_single_crack.cub

	if [ -n "$2" ] 
	 then
	  echo "File name: $2"
	  file=$2
  fi

../tools/mofem_part -my_file $file -my_nparts $proc
for ((ref=0; ref <=7-$order; ref++ ))
do

echo New calculations. Order: $order Ref: $ref

mpirun -mca oob_tcp_keepalive_time 0 -np $proc ./crack_propagation -my_file out.h5m  -snes_monitor -snes_atol 1e-8 -snes_rtol 0 -snes_converged_reason -snes_linesearch_monitor  -ksp_monitor -ksp_type fgmres -my_max_post_proc_ref_level 0 -ksp_atol 1e-12 -ksp_rtol 0 -my_order $order -mofem_mg_verbose 1 -mofem_mg_coarse_order 1 -mofem_mg_levels $order -mg_coarse_ksp_type preonly -mg_coarse_pc_type lu -mg_coarse_pc_factor_mat_solver_package mumps -pc_mg_smoothup 10 -pc_mg_smoothdown 10 -pc_mg_galerkin true -pc_mg_type multiplicative -pc_type lu -pc_factor_mat_solver_package mumps -my_ref 0 -my_geom_order 1 -my_ref_order $ref -my_add_singularity 0 -material HOOKE -nb_load_steps 1 -load_scale 1 -log_summary  2>&1 | tee log0

#stress intensity factor
grep "griffith force at ent" log0 | awk 'BEGIN { gc = 0 } { gc += $11 } END {printf "%d %6.4e %4.3f\n",NR,gc/NR,((sqrt(1000*gc/NR)/1.949936442)-1)*100} ' | awk '{print $3}' | tee log_err

#dofs
grep "name ELASTIC Nb. local dof" log0 | grep "rank = 0" | grep "row" | awk '{print $25}' | tee log_dof

#time
grep "Time (sec):" log0 | awk '{print $3}' | tee log_time

#energ
grep "Elastic energy" log0 | awk '{print $3}' | tee log_energ

echo $order $ref "$(<log_dof)" "$(<log_err)" "$(<log_energ)" "$(<log_time)" | tee -a log_result

mbconvert out_spatial_0.h5m out_spatial_0_${ref}_${order}.vtk

done
done

elif [ "$1" = 2 ]
then

echo Argument is equal to ${1}


  if [ -n "$2" ]
   then
    echo "material model: $2"
  else 
	echo "NO MATERIAL SPECIFIED!"
	return 0
  fi

proc=8
# order=6
# ref=1
singularity=0

if [ "$3" = "singularity" ]
then
  singularity=1
  echo "WITH SINGULARITY"
else 
  echo "NO SINGULARITY"
fi

rm log_total_$singularity

for order in 1 2 3 4 5 6
do

for ((ref=0; ref <=7-$order; ref++ ))
 do
 rm log_result
 
 for file in plate_with_single_crack200.cub plate_with_single_crack198.cub plate_with_single_crack196.cub
  do
  ../tools/mofem_part -my_file $file -my_nparts $proc
  file_name=${file%.cub}
  
  echo File: $file | tee -a log_result
  
  mpirun -mca oob_tcp_keepalive_time 0 -np $proc ./crack_propagation -my_file out.h5m  -snes_monitor -snes_atol 1e-8 -snes_rtol 0 -snes_converged_reason -snes_linesearch_monitor  -ksp_monitor -ksp_type fgmres -my_max_post_proc_ref_level 0 -ksp_atol 1e-12 -ksp_rtol 0 -my_order $order -mofem_mg_verbose 1 -mofem_mg_coarse_order 1 -mofem_mg_levels $order -mg_coarse_ksp_type preonly -mg_coarse_pc_type lu -mg_coarse_pc_factor_mat_solver_package mumps -pc_mg_smoothup 10 -pc_mg_smoothdown 10 -pc_mg_galerkin true -pc_mg_type multiplicative -pc_type lu -pc_factor_mat_solver_package mumps -my_ref 0 -my_geom_order 1 -my_ref_order $ref -material $2 \
  -my_add_singularity $singularity -my_max_post_proc_ref_level 0 -my_n_bone 1 -my_rho0 1 -nb_load_steps 1 -load_scale 1 \
  -mwls_dm 4 -mwls_number_of_base_functions 4 -scaleMHD 1 -my_mwls_approx_file cube_for_test_2Y.mhd -my_density_block 1 \
  -log_summary  2>&1 | tee log_${file_name}
  
  #stress intensity factor
  grep "griffith force at ent" log_${file_name} | awk '{print $11}' | tee log_err
  
  #dofs
  dofss=$(grep "name ELASTIC Nb. local dof" log_${file_name} | grep "rank = 0" | grep "row" | awk '{print $25}' )
  
  #time
  #grep "Time (sec):" log0 | awk '{print $3}' | tee log_time
  
  crack_length=${file#"plate_with_single_crack"}
  crack_length=${crack_length%.cub}
  crack_length=${crack_length#0}
  
  #energ
  grep "Elastic energy" log_${file_name} | awk '{print $3}' | tee log_energ
  
  echo $order $ref $crack_length $'\n'energy "$(<log_energ)" $'\n'"$(<log_err)" | tee -a log_result
  
  mbconvert out_spatial_0.h5m out_spatial_0_${file_name}.vtk
 done
 
 error=$(grep "" log_result | awk 'BEGIN { var  = 0; ct =0; eng1=0; eng2=0; } NR==3{eng1=$2} NR>10 && NR<15{ var += $1; ct +=1 } NR==17{eng2=$2} END {printf "%6.4g %6.4g\n",2*var/ct,sqrt((eng1-eng2)*(eng1-eng2))/0.02} ' | awk '{printf "%6.4g\n", 100*($2-$1)/$2} ')
 
 echo $order $ref $error $dofss | tee -a log_total_$singularity 
 
 done
done

elif [ "$1" = 3 ]
 then
 
 echo "Argument 3 (not implemented yet)"
else
 echo "Provide correct number of the case."

fi
