#Useful commands
### Start shell script
~~~~
#!/bin/sh
~~~~

##GNUPLOT, data prepare
~~~~
grep "Elastic" log | awk '{print $2,$4,$7}' | tee log_upd 
~~~~
~~~~
{ FS = ",[ \t]*|[ \t]+" } { print $2, $1 } { s += $1 } { print "sum is", s, " average is", s/NR }
~~~~
~~~~
gnuplot -e "plot "log_upd" using 1:2 title 'psi' with lines"
plot "log_upd" using 1:2 title 'psi' with lines
~~~~
##SYNC *cub files
~~~~
rsync -avz -a --include '*/' --include '*.cub' --exclude '*' $MOFEM_INSTALL_DIR/users_modules/ karol@rdb-srv1.eng.gla.ac.uk:~/mofem_installation/users_modules/
~~~~

##COPY file to server
~~~~
ssh -X karol@rdb-srv1.eng.gla.ac.uk
~~~~
####Download from server
~~~~
scp karol@rdb-srv1.eng.gla.ac.uk:~/users_modules/bone_remodelling/
~~~~
####Upload to server
~~~~
scp ./dog_bone.cub karol@rdb-srv1.eng.gla.ac.uk:~/users_modules/bone_remodelling/
~~~~

##FOLDER SIZES
~~~~
du -sh ./*
~~~~
##SERVER CALCULATIONS with *screen*
~~~~
screen -ls
#only execute command
screen -dm ./my_command
#GIVE THE NAME AND CLOSE AFTER ITS DONE
screen -S screenname -dm ./my_command
screen -XS screenname quit
ctrl +a, ctrl +d,
screen -r screenname
"screen -r screenname"

#DELETE SCREEN
screen -XS "name" quit
~~~~
##MOFEM
~~~~
if boost library is not working
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/local_boost_1_54_0/lib/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/local_boost_1_65_1/lib/

synchronize mofem on server
rsync -avz --delete-after $MOFEM_INSTALL_DIR/mofem-cephas/ karol@rdb-srv1.eng.gla.ac.uk:~/mofem_installation/mofem-cephas/

~~~~

##PROXY on the server
~~~~
export https_proxy=http://wwwcache.gla.ac.uk:8080/
export https_proxy=https://wwwcache.gla.ac.uk:8080/
~~~~

##DEBUGGING
~~~~
valgrind --dsymutil=yes --track-origins=yes --trace-children=yes ./command

lldb -- ./command
~~~~

##DELETE FILES, CHANGE NAMES, ETC
~~~~
directory sizes: du -sh */

find . -type l -newermt "Aug 7 22:27" -delete
find . -name "*_tractions.vtk" -type f ! -newermt "Jan 14 22:27" -delete

find ./ -name "*.txt" | xargs -I '{}' basename '{}' | sed 's/\.txt//' | xargs -I '{}' mv '{}.txt'  '{}.md'
~~~~

##SET SSH PASSWORD AUTOMATICALLY
~~~~
if you run this command and when it prompts for a password leave it blank: `ssh-keygen -t rsa`
 
then run this command `cat ~/.ssh/id_rsa.pub | ssh karol@rdb-srv1.eng.gla.ac.uk "mkdir -p ~/.ssh && cat >>  ~/.ssh/authorized_keys"  `
~~~~

##SLOW EXCEL OR POWERPOINT, no worries!
~~~~
sudo update_dyld_shared_cache -force

CHECK DEVICES IN THE NETWORK!!
arp -a | grep :
~~~~

##CHANGE COMMAND FOR JSON ARGUMENTS (ADD QUOTATION MARKS) debug
~~~~
printf '"%s", \n' ${put_your_command_here}
~~~~


##MOUNT SSH FILE SYSTEM 
~~~~
install FUSE for macOS and SSHFS (`https://osxfuse.github.io/`)
mkdir ~/Desktop/rdb-srv1
sudo fuser -c /Users/karollewandowski/Desktop/rdb-srv1
sshfs karol@rdb-srv1.eng.gla.ac.uk:/home/karol/ /Users/karollewandowski/Desktop/rdb-srv1

Optionally:

echo "alias server_mount='sshfs karol@rdb-srv1.eng.gla.ac.uk:/home/karol/ /Users/karollewandowski/Desktop/rdb-srv1'" >> ~/.bash_profile
~~~~

##PARTITION SSD ON a MAC
~~~~
diskutil erasedisk hfs+ External GPT /dev/disk2
~~~~

##INSTALL MACOS ON USBSTICK
~~~~
sudo /Volumes/STORAGE/installation_files/Install\ macOS\ Sierra.app/Contents/Resources/createinstallmedia --volume /Volumes/SANDISK/ --applicationpath /Volumes/STORAGE/installation_files/Install\ macOS\ Sierra.app
~~~~
##UPAUSE ALL PAUSED APPS ON MACOS
~~~~
pkill -CONT -u $UID
~~~~

##EXECUTING TIME FOR program
~~~~
/usr/bin/time $your command :)
~~~~

##BREAK THE PARTICULAR LINES IN LLDB
~~~~
b -f mwls_approx.cpp -l 65
~~~~

##MOUNT AN IMAGE ON A DRIVE
~~~~
diskutil list
sudo dd bs=1m if=image.img of=/dev/rdisk<disk# from diskutil> conv=sync
~~~~

##CLONE USB DRIVE ON LINUX
~~~~
diskutil list
sudo dd if=/dev/sd??? of=/dev/sd??? status=progress bs=65536 conv=sync

sudo dd if=/dev/sd2 of=usb.img bs=65536

sudo dd if=/dev/disk? of=/dev/disk bs=65536

(press ctrl + t to check the status)
~~~~

##MAKE A GIF
~~~~
convert -delay 20 -loop 0 *.jpg myimage.gif
~~~~
##CONVERT A VIDEO FILE FILE JPEG
~~~~
ffmpeg -i media8.mov media8.mp4
ffmpeg -y -r 24 -i seeing_noaudio.mp4 seeing.mp4

ffmpeg -f image2 -framerate 60 -pattern_type sequence -start_number 0000 -i my_vid.%04d.jpeg my_video.mp4

ffmpeg -i myvid%03d.png -pix_fmt yuv420p movie.mp4
#transparent video
ffmpeg -framerate 25 -i myvid.%04d.png -vcodec png z.mov && ffmpeg -i z.mov z.mp4
ffmpeg -i my-video.mov -vcodec h264 -acodec mp2 my-video.mp4

this works with iMovie
ffmpeg -framerate 60 -i my_vid.%04d.png   -vcodec qtrle movie_with_alpha.mov
reduces size:
ffmpeg -i lab2_fem.mp4   -vcodec libx264 -crf 28 lab2_fem_red.mp4 
~~~~

#transparent video
ffmpeg -framerate 25 -i myvid.%04d.png -vcodec png z.mov
~~~~
##PRINT FILE WITH NUMBER OF COLUMNS
~~~~
awk '{print NR " "  $0}' log_upd 
cat -n log_pad > save_here.txt
~~~~

##Use shell script directly
~~~~
cp make_command_arguments /usr/local/bin/
~~~~

##Time profiling on MAC
~~~~
instruments -v -t "Time Profiler" $COMMAND
~~~~

##Add a link to binary into /usr/local/bin (I did it with cubit and paraview)
~~~~
ln -s /Applications/ParaView-5.5.0.app/Contents/MacOS/paraview /usr/local/bin
~~~~

##Reset git merge
~~~~
git reset --merge ORIG_HEAD
~~~~

##Convert svg with latex in Inkscape command
~~~~
cd /Applications/Inkscape.app/Contents/Resources/bin/
.inkscape  -D -z --file=/Users/karollewandowski/Desktop/image46561.svg  --export-pdf=/Users/karollewandowski/Desktop/release_bone_energy_paper_2019/Figures//tikz/$file  --export-latex
~~~~

##Search for lines between particular lines
~~~~
awk '/User-provided matrix/{flag=1;next}/Explicit/{flag=0}flag' log | grep "row " | awk -F"[()]" '{print $2}'
~~~~

##Print words containing a string '43' (regex) and one string after that
~~~~
awk '{for(i=1;i<=NF;i++){ if(match($i, /'43'/)){print $i $(i+1)} } }'
~~~~

~~~~
awk '/User-provided matrix/{flag=1;next}/Explicit/{flag=0}flag' log | grep "row " | sed 's/)*//g' | tr -s " " | cut -d " " -f4- | awk '{for(x=1;x<=NF;x++)if(x % 2)printf "%s", $x (x == NF || x == (NF-1)?"\n":" ")}' | column -t | tee log_out
~~~~
##Configure users modules with spack (Courtesy of Lukasz)
~~~~
./spconfig.py -DMOFEM_DIR=../um_view $HOME/mofem_install/mofem-cephas/mofem/users_modules -DMOFEM_UM_BUILD_TESTS=ON -DWITH_METAIO=1 -DFM_VERSION_MAJOR=0 -DFM_VERSION_MINOR=0 -DFM_VERSION_BUILD=0
~~~~

##Path to the newest file
~~~~
$(ls -t |head -n1)
~~~~

##MBCONVERT in parallel
~~~~
parallel -j6 mbconvertpr ::: out_*h5m
~~~~

##SHOW HIDDEN FILES IN FINDER
~~~~
shift + command + .
~~~~

##RSYNC source with the server
~~~~
rsync -razP ~/moFEM/mofem-cephas/mofem/users_modules/fracture_mechanics/ $server/mofem_install/mofem-cephas/mofem/users_modules/fracture_mechanics
~~~~

##PATH TO THE NEWEST FILE
~~~~
my_last_file=$(ls -l out_*h5m | sort -n | tail -1 | awk '{print $9}')
~~~~

##USE BLENDER TO FIX FILES IN A BATCH
~~~~
/Applications/blender.app/Contents/MacOS/blender -b -P /Users/karollewandowski/Dropbox/Blender/blender_bake_vertex_colors.py -- my_*.obj
for f in *.ply; do /Applications/blender.app/Contents/MacOS/blender -b -P /Users/karollewandowski/Dropbox/Blender/blender_bake_vertex_colors.py -- $f; done
~~~~

##ADD 000 padding to the numbering (files crack_only_??.obj)
```
for a in crack_only_*.obj; do
  c=${a#crack_only_}
  c=${c%.obj}
  c=$(printf %04d.obj ${c%.obj})
  c=crack_only_$c
  if [ $a != $c ]; then
    mv $a $c
  fi
done
```


##DELETE ALL THE FILES EXCEPT THOSE ENDING WITH 5 and 0
```
rm -f *[12346789].png
```

##SLACK ANALYSIS CRASH REPORT
```
if [ ! ${PIPESTATUS[0]} -eq 0 ]; then
          curl -X POST -H 'Content-type: application/json' --data '{"text":"My analysis crashed"}' https:hooks.slack.com/services/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
fi

```
##EXCLUDE FOLDER FROM TIME MACHINE
```
sudo tmutil addexclusion /Users/karollewandowski/moFEM/mofem-cephas/mofem/users_modules/.vscode/ipch
```

##START GIT REPO
```
git init
git add --all
git commit -m 'first commit'
git remote add origin https://karol41@bitbucket.org/karol41/karol_phd_thesis2020.git
git push -u origin --all

```
##REMAP A KEY IN MACOS
```
https://github.com/pqrs-org/Karabiner-Elements/issues/2196#issuecomment-603663747
https://www.nanoant.com/mac/macos-function-key-remapping-with-hidutil
https://developer.apple.com/library/archive/technotes/tn2450/_index.html

hidutil property --set '{"UserKeyMapping":[{"HIDKeyboardModifierMappingSrc":0x7000000E4,"HIDKeyboardModifierMappingDst":0x7000000E6}]}'

hidutil property --set '{"UserKeyMapping":[{"HIDKeyboardModifierMappingSrc":0x700000064,"HIDKeyboardModifierMappingDst":0x700000029}]}'

hidutil property --get "UserKeyMapping"
```

##INSTALL WITH SPACK
```
spack install --only dependencies mofem-cephas+slepc ^petsc+X
spack install octave +gnuplot && spack install parallel 
spack view --verbose symlink -i spack_view $PACKAGE
```
##CHECK MY EXTERNAL IP ADDRESS
```
curl ifconfig.me 
```
##MAKE XQUARTZ WORK WITH SALOME
```
defaults write org.macosforge.xquartz.X11 enable_iglx -bool true 
~/Downloads/ParaView-5.7.0-MPI-Linux-Python3.7-64bit/bin/pvserver -display localhost:0
```
##TEAMS webHOOK
```
TITLE="Title of your message"
MESSAGE="Body of your message"
JSON="{\"title\": \"${TITLE}\", \"text\": \"${MESSAGE}\" }"
WEBHOOK_URL="https://outlook.office.com/webhook/43c9f281-c5e1-4833-be78-27addb5d8529@6e725c29-763a-4f50-81f2-2e254f0133c8/IncomingWebhook/441a73f641534b14ac0d65ead4adb94b/4b2fd8be-d98a-492f-b349-c2ae11c04573"
```
##TEAMS debug with mpi (parallel) on linux, cactus
```
mpirun  --use-hwthread-cpus -np 4 xterm -e gdb my_mpi_application
```

## Reclaim purge-able storage
```
dd if=/dev/random of=~/largefile bs=45m
```

## App dark mode add this to Info.plist
```
  <key>NSRequiresAquaSystemAppearance</key>
  <true/>
```

Remove code signature (faster vscode on big sur)
```
codesign --remove-signature /Applications/Visual\ Studio\ Code.app/Contents/Frameworks/Code\ Helper\ \(Renderer\).app
```

Find spack path automagically
```
echo $(spack find -l --path moab | awk 'END{print}' | awk 'NF{ print $NF }')""
```

