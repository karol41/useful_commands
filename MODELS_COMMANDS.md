#BONE REMODELLING
####simple bar
~~~~
mpirun -mca oob_tcp_keepalive_time 0 -np 4 ./bone_adaptation -my_file simple_bar.cub -my_order 2 -my_output_prt 1 -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_atol 1e-12 -ksp_rtol 1e-12 -snes_monitor -snes_type newtonls -snes_linesearch_type basic -snes_max_it 10 -snes_atol 1e-8 -snes_rtol 1e-8 -ts_type beuler -ts_dt 0.1 -ts_final_time 8 -ts_max_snes_failures -1 -my_load_history load_history.in -my_config my_config_kuhl2.in -mass_postproc -ts_monitor | tee log
~~~~

####frame simple optimisation
~~~~
mpirun -mca oob_tcp_keepalive_time 0 -np 4 ./bone_adaptation -my_file frame_bike.cub -my_order 2 -my_output_prt 5 -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_atol 1e-12 -ksp_rtol 1e-12 -snes_monitor -snes_type newtonls -snes_linesearch_type basic -snes_max_it 20 -snes_atol 1e-8 -snes_rtol 1e-8 -ts_type beuler -ts_dt 0.01 -ts_final_time 1 -ts_max_snes_failures -1 -my_load_history load_history2.in -young_modulus 1000 -poisson_ratio 0.3 -rho_ref 0.1 -psi_ref 2.0 -c 1.0 -m 3 -n 2 -mass_postproc -ts_monitor | tee log
~~~~


####dog bone2
~~~~
mpirun -mca oob_tcp_keepalive_time 0 -np 4 ../bone_adaptation -my_file ../dog_bone0.cub -my_order 2 -my_output_prt 10 -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_atol 1e-12 -ksp_rtol 1e-12 -snes_monitor -snes_type newtonls -snes_linesearch_type basic -snes_max_it 20 -snes_atol 1e-8 -snes_rtol 1e-8 -ts_type beuler -ts_dt 0.01 -ts_final_time 1 -ts_max_snes_failures -1 -my_load_history ../load_history2.in -my_config ../my_config_kuhl2.in -mass_postproc -young_modulus 10000 -poisson_ratio 0.3 -rho_ref 0.1 -psi_ref 1.0 -c 1.0 -r0 -0.1  -m 3 -n 2 -ts_monitor | tee log
~~~~

####MC3 2d
~~~~
mpirun -mca oob_tcp_keepalive_time 0 -np 8 ./bone_adaptation -my_file ./examples/femur_2d.h5m -my_order 2 -my_output_prt 10 \
-ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_atol 1e-12 -ksp_rtol 1e-12 -snes_monitor -snes_type newtonls \
-snes_linesearch_type basic -snes_max_it 30 -snes_atol 1e-6 -snes_rtol 1e-8 -ts_type beuler -ts_max_snes_failures 1 \
-ts_dt 0.1 -ts_final_time 500 \
-my_load_history load_history.in -mass_postproc -ts_monitor \
-young_modulus 600 -poisson_ratio 0.2 -c 1 -rho_ref 1.0 -psi_ref 0.01 -m 3.25 -n 2.25 | tee log
~~~~

####MC3 3D NEW NEW
~~~~
mpirun -np ${proc} ./bone_adaptation -my_file out.h5m -my_order ${order} -my_output_prt 1 -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_atol 1e-12 -ksp_rtol 1e-12 -snes_monitor -snes_type newtonls -snes_linesearch_type basic -snes_max_it 30 -snes_atol 1e-6 -snes_rtol 1e-8 -ts_type beuler -ts_max_snes_failures -1 -snes_max_linear_solve_fail 1000 -ts_dt 0.5 -ts_final_time 10 -my_load_history load_history3.in -mass_postproc -ts_monitor -young_modulus 900 -poisson_ratio 0.3 -c $c -rho_ref 1 -psi_ref 0.0025 -m 3 -n 2 -equilibrium_stop_rate 0.001 -log_summary \
-ksp_type gmres -pc_type fieldsplit \
-ksp_atol 1e-8 -ksp_rtol 1e-8 \
-ksp_max_it 1000 \
-fieldsplit_0_ksp_type preonly \
-fieldsplit_0_pc_type lu \
-fieldsplit_0_pc_factor_mat_solver_package mumps \
-fieldsplit_1_ksp_type preonly \
-fieldsplit_1_pc_type lu \
-fieldsplit_1_pc_factor_mat_solver_package mumps \
-pc_fieldsplit_type multiplicative | tee log_${file%.cub}_ord${order}
~~~~



####FEMUR
~~~~
../tools/mofem_part -my_file ./femur_implant_3d/implant_3d_mesh.trelis -my_nparts 16 
mpirun -np 16 ./bone_adaptation \
-my_file out.h5m -my_order 2 -my_output_prt 10 \
-ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps \
-ksp_atol 1e-12 -ksp_rtol 1e-12 -snes_monitor -snes_type newtonls \
-snes_linesearch_type basic \
-snes_max_it 1000 -snes_atol 1e-6 -snes_rtol 1e-8 \
-ts_type beuler -ts_max_snes_failures 1 \
-ts_dt 0.1 -ts_final_time 20 -my_load_history ./examples/load_history2.in \
-mass_postproc -ts_monitor -young_modulus 500 -poisson_ratio 0.2 -c 1 \
-rho_ref 1.2 -psi_ref 0.01 -m 3 -n 2 | tee log
~~~~

####FEMUR with implant 3D (abaqus
~~~~
../tools/mofem_part -my_file ./femur_implant_test.cub -my_nparts 8 && \
mpirun -np 8 ./bone_adaptation \
-my_file out.h5m -my_order 2 -my_output_prt 10 \
-ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps \
-ksp_atol 1e-12 -ksp_rtol 1e-12 -snes_monitor -snes_type newtonls \
-snes_linesearch_type basic \
-snes_max_it 1000 -snes_atol 1e-6 -snes_rtol 1e-8 \
-ts_type beuler -ts_max_snes_failures 1 \
-ts_dt 0.1 -ts_final_time 20 -my_load_history ./examples/load_history2.in \
-mass_postproc -ts_monitor -young_modulus 3800 -poisson_ratio 0.3 -c 1 \
-rho_ref 1e-9 -psi_ref 0.00275 -m 3 -n 2 | tee log
~~~~

###ADAPTIVE TS (atom example)
~~~~
/Users/karollewandowski/moFEM/petsc/arch-darwin-c-opt/bin/mpirun "-np" "2" "/Users/karollewandowski/moFEM/users_modules/bone_remodelling/bone_adaptation" "-my_file" "/Users/karollewandowski/moFEM/mofem-cephas/mofem/users_modules/bone_remodelling/examples/atom_test_adap.h5m" "-my_order" "2" "-my_output_prt" "100" "-ksp_type" "fgmres" "-pc_type" "lu" "-pc_factor_mat_solver_package" "mumps" "-ksp_atol" "1e-12" "-ksp_rtol" "1e-12" "-snes_type" "newtonls" "-snes_linesearch_type" "basic" "-snes_max_it" "10" "-snes_atol" "1e-6" "-snes_rtol" "1e-8" -ts_type beuler -ts_dt 0.1  "-ts_final_time" "5" "-ts_max_snes_failures" "1" "-my_load_history" "/Users/karollewandowski/moFEM/mofem-cephas/mofem/users_modules/bone_remodelling/examples/load_history2.in" "-mass_postproc" "-young_modulus" "0.5" "-poisson_ratio" "0" "-c" "1" "-psi_ref" "1" "-rho_ref" "1" "-n" "2" "-m" "3" -ts_adapt_type basic -ts_adapt_dt_max 1.0 -ts_adapt_dt_min 0.01 -ts_rtol 0.01 -ts_atol 0.01 -ts_adapt_basic_safety 0.8 -ts_adapt_monitor
~~~~


###docker on server
~~~~
docker run --rm=true -it --volumes-from mofem_build  -v $HOME/mofem-cephas/mofem:/mofem -v $HOME:$HOME -e HOSTHOME=$HOME mofem_build_petsc_3.8.3 /bin/bash
~~~~


###FEMUR implant (ADAPTIVE)
~~~~
../tools/mofem_part -my_file $HOSTHOME/bone_remodelling/femur_implant_test.cub -my_nparts 11 && mpirun -np 11 ./bone_adaptation -my_file out.h5m -my_order 2 -my_output_prt 10 -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -ksp_atol 1e-12 -ksp_rtol 1e-12 -snes_monitor -snes_type newtonls -snes_linesearch_type basic -snes_max_it 1000 -snes_atol 1e-6 -snes_rtol 1e-8 -ts_type beuler -ts_max_snes_failures 1 -ts_dt 0.1 -ts_final_time 100 -my_load_history $HOSTHOME/bone_remodelling/load_history_femur.in -less_postproc -mass_postproc -ts_monitor -young_modulus 3800 -poisson_ratio 0.3 -c 1 -rho_ref 0.3 -psi_ref 0.00275 -m 3 -n 2 -ksp_monitor -ts_adapt_type basic -ts_adapt_dt_max 5.0 -ts_adapt_dt_min 0.01 -ts_rtol 0.01 -ts_atol 0.01 -ts_adapt_basic_safety 0.8 -ts_adapt_monitor | tee log
~~~~

#FRACTURE MECHANICS

###CRACK PROPAGATION LOAD-DISP CURVE
~~~~
grep -E "(Propgation step|F_lambda2)" log | grep -v "Not Converged Propgation" | awk 'BEGIN {print "-1 0 0"} /F_lambda2 / { lambda = $14 } /Propgation step/ { F=lambda;  print $3,2*$9/F,F }' | tee gp_3nd
~~~~